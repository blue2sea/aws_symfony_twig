<?php

use Silex\Application;
use WellGedaan\Indova\Application\Controller\HiscoreController;
use WellGedaan\Indova\Application\Controller\PageController;
use WellGedaan\Indova\Application\Controller\SecurityController;
use WellGedaan\Indova\Application\Controller\StoreController;
use WellGedaan\Indova\Application\Controller\VoteController;
use WellGedaan\Indova\Application\Provider\ServerUpdateProvider;
use WellGedaan\Indova\Infrastructure\ServiceProvider\ConfigServiceProvider;
use WellGedaan\Indova\Infrastructure\ServiceProvider\DatabaseServiceProvider;
use WellGedaan\Indova\Infrastructure\ServiceProvider\SecurityServiceProvider;
use WellGedaan\Indova\Infrastructure\ServiceProvider\TwigServiceProvider;

require_once __DIR__ . "/bootstrap.php";

$app = new Application();
$app['debug'] = true;

$app->register(new ConfigServiceProvider);
$app->register(new DatabaseServiceProvider);
$app->register(new TwigServiceProvider);
$app->register(new SecurityServiceProvider);

$app->register(new ServerUpdateProvider);

$app->boot();

$app->mount('/', new PageController(
    $app['twig'],
    $app['server.update.repository']
));

$app->mount('/store', new StoreController(
    $app['twig'],
    $app['orm.em']
));

$app->mount('/vote', new VoteController(
    $app['twig'],
    $app['orm.em']
));

$app->mount('/hiscores', new HiscoreController(
    $app['twig']
));

$app->mount('/security', new SecurityController(
    $app['twig'],
    $app['orm.em'],
    $app['security.last_error']
));

return $app;
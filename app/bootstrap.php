<?php
require_once __DIR__ . "/../vendor/autoload.php";

define('APP_PATH', __DIR__);
define('VIEW_PATH', __DIR__ . '/../resources/views');

define('CONFIG_PATH', __DIR__ . "/config");
define('CACHE_PATH', __DIR__ . "/cache");
jQuery(document).ready(function($) {
    var categoriesWrapper = $('.categories-wrapper');
    var itemsWrapper = $('.categories-items-wrapper');
    var currentCategoryItemList = null;
    var basketList = $('#store-basket');
    var basketTotal = $('#store-basket-total');
    var basket = JSON.parse(localStorage.getItem('basket'));
    basket = basket ? basket : {};

    function bindEventListeners() {
        $('.single-category').click(handleSingleCategoryClick);
        $('#back-button').click(handleBackButton);
        $('.category-item-list').click(addItemToCart);
    }

    function handleSingleCategoryClick() {
        categoriesWrapper.hide();
        currentCategoryItemList =  $(this).data('id');

        itemsWrapper.show();
        $('#item-list-' + currentCategoryItemList).show();
    }

    function handleBackButton() {
        $('#item-list-' + currentCategoryItemList).hide();
        itemsWrapper.hide();
        categoriesWrapper.show();
    }

    function addItemToCart() {
        var name = $(this).data('name');
        var price = parseInt($(this).data('price'));
        var id = $(this).data('id');

        basket[id] = {
            name: name,
            price: price
        };

        updateBasket();
    }

    function removeFromCart() {
        console.log("HI");
        var id = $(this).data('id');
        delete basket[id];

        updateBasket();
    }

    function updateBasket() {
        var innerBasket = '';
        var totalPrice = 0;
        for (var index in basket) {
            innerBasket += "<li class='basket-item' data-id='" + index + "'> " + basket[index].name + " </li>"

            totalPrice += basket[index].price;
        }

        basketList.html(innerBasket);
        basketTotal.html(totalPrice);
        localStorage.setItem('basket', JSON.stringify(basket));

        $('.basket-item').click(removeFromCart);
    }

    bindEventListeners();
    updateBasket();
});
jQuery(document).ready(function() {

    var voted = 0;
    var voteSessionKey = localStorage.getItem("indova_vote_session");

    $('.single-toplist button').click(function() {
        var button = $(this);
        var url = $(this).data('link');

        url += voteSessionKey;

        var id = $(this).data('id');

        button.html(button.data('loading-text'));
        window.open(url);

        checkVoteStatus(id, button, false);
    });

    $('.single-toplist button').each(function() {
        var id = $(this).data('id');
        console.log(id);
        checkVoteStatus(id, $(this), true);
    });

    function checkVoteStatus(id, button, initial) {
        $.ajax({
            url: '/vote/check-voted',
            method: 'POST',
            data: {
                id: id,
                voteSessionKey: voteSessionKey
            },
            error: function(data) {
                console.log(data);
            },
            success: function(data) {
                console.log(data);

                if(data.voted === false) {
                    if(initial === false) {
                        setTimeout(function () {
                            checkVoteStatus(id, button, false);
                        }, 2000);
                    }
                } else if(data.voted === true) {
                    button.html(button.data('done-text'));
                    voted++;
                }
            }
        });
    }


    $('#claim-form').submit(function() {
        var username = $('#vote-username').val();

        $.ajax({
            url: '/vote/claim',
            method: 'POST',
            data: {
                username: username,
                voteSessionKey: voteSessionKey
            },
            error: function(data) {
                console.log(data);
                triggerNotification("Something went wrong while claiming your reward!")
            },
            success: function(data) {
                console.log(data);
                triggerNotification(data.message)
            }
        });

        return false;
    });

    function triggerNotification(message) {
        $('#vote-message').html(message);
    }

    function checkVoteSession() {
        if(!voteSessionKey){
            voteSessionKey = makeid();
            localStorage.setItem("indova_vote_session", voteSessionKey);
        }

    }

    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < 20; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }


    checkVoteSession();
});
<?php

namespace WellGedaan\Indova\Application\Controller;

use Doctrine\ORM\EntityManager;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WellGedaan\Indova\Domain\Vote\Callback;
use WellGedaan\Indova\Domain\Vote\Reward;

class VoteController implements ControllerProviderInterface
{

    const RUNELOCUS = 6;
    CONST RSPSLIST = 9;
    CONST TOPG = 25;

    const TOPLISTS = [
        "6" => [
            "id" => 6,
            "name" => "RuneLocus",
            "link" => "http://www.runelocus.com/top-rsps-list/vote-44969/?id2=",
            "rewards" => [
                "5022" => "Vote ticket",
                "979" => "Vote present"
            ]
        ],
        "9" => [
            "id" => 9,
            "name" => "RSPS-List",
            "link" => "http://www.rsps-list.com/index.php?a=in&u=indova&id=",
            "rewards" => [
                "5022" => "Vote ticket"
            ]
        ],
        "25" => [
            "id" => 25,
            "name" => "TopG",
            "link" => "http://topg.org/Runescape/in-478699-",
            "rewards" => [
                "5022" => "Vote ticket",
            ]
        ]
    ];


    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(\Twig_Environment $twig, EntityManager $entityManager)
    {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
    }


    /**
     * {@inheritdoc}
     */
    public function connect(Application $app): ControllerCollection
    {
        /** @var ControllerCollection $controllers */
        $controllers = $app['controllers_factory'];

        $controllers->get('/', [$this, 'renderVoteIndex'])->bind('vote.index');
        $controllers->post("/check-voted", [$this, 'checkVoteStatus'])->bind('vote.check');
        $controllers->post('/claim', [$this, 'claimVote'])->bind('vote.claim');

        $controllers->match("/WSPKr4wogacallback", [$this, 'handleRuneLocusCallback']);
        $controllers->match("/k4zHcY7pI5callback", [$this, 'handleTopGCallback']);
        $controllers->match('/lmc88RtU15callback', [$this, 'handleRSPSListCallback']);

        return $controllers;
    }


    public function renderVoteIndex(): Response
    {
        return new Response(
            $this->twig->render('vote/vote.html.twig', [
                'toplists' => self::TOPLISTS
            ])
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function checkVoteStatus(Request $request)
    {
        $id = $request->get('id');
        $sessionKey = $request->get('voteSessionKey');

        if ($id == null || $id == "") {
            return new JsonResponse([
                "error" => "please provide an id"
            ]);
        }

        $topLists = self::TOPLISTS;

        //checks if the id is present in the Toplists
        if (!isset($topLists[$id])) {
            return new JsonResponse([
                "voted" => false,
                "toplist_id" => $id
            ]);
        }

        $currentToplist = $topLists[$id];

        if ($currentToplist['name'] == "TopG") {
            return $this->handleTopG($sessionKey);
        } else if ($currentToplist['name'] == "RuneLocus") {
            return $this->handleRuneLocus($sessionKey);
        } else if ($currentToplist['name'] == "RSPS-List") {
            return $this->handleRSPSList($sessionKey);
        }




        return new JsonResponse([
            "voted" => false
        ]);
    }

    /**
     * @param $sessionId
     * @return JsonResponse
     */
    private function handleRSPSList($sessionId)
    {
        /** @var Callback $callBack */
        $callBack = $this->entityManager->getRepository(Callback::class)->findOneBy([
            "sessionId" => $sessionId,
            "topListId" => self::RSPSLIST,
            "status" => false
        ], ['date' => 'DESC']);

        if ($callBack != null && (time() - $callBack->getDate()->getTimestamp()) < 3600 * 12) {
            return new JsonResponse([
                "voted" => true
            ]);
        }
        return new JsonResponse([
            "voted" => false
        ]);
    }

    /**
     * @param $sessionId
     * @return JsonResponse
     */
    private function handleRuneLocus($sessionId)
    {
        /** @var Callback $callBack */
        $callBack = $this->entityManager->getRepository(Callback::class)->findOneBy([
            "sessionId" => $sessionId,
            "topListId" => self::RUNELOCUS,
            "status" => false
        ], ['date' => 'DESC']);

        if ($callBack != null && (time() - $callBack->getDate()->getTimestamp()) < 3600 * 12) {
            return new JsonResponse([
                "voted" => true
            ]);
        }

        return new JsonResponse([
            "voted" => false
        ]);
    }

    /**
     * @param $sessionId
     * @return JsonResponse
     */
    private function handleTopG($sessionId)
    {
        /** @var Callback $callBack */
        $callBack = $this->entityManager->getRepository(Callback::class)->findOneBy([
            "sessionId" => $sessionId,
            "topListId" => self::TOPG,
            "status" => false
        ], ['date' => 'DESC']);

        $voted = (int)file_get_contents("http://topg.org/check_ip.php?siteid=478699&userip=" . $_SERVER['REMOTE_ADDR']);
        if ($voted == 1 && $callBack == null) {
            return $this->handleCallbackInserting(self::TOPG, $sessionId);
        }

        if ($callBack != null && (time() - $callBack->getDate()->getTimestamp()) < 3600 * 12) {
            return new JsonResponse([
                "voted" => true
            ]);
        }

        return new JsonResponse([
            "voted" => false
        ]);
    }

    public function handleCallbackInserting($topListId, $sessionId)
    {
        $callback = new Callback();
        $callback->setSessionId(
            $sessionId != null ? $sessionId : "NULL"
        );

        $callback->setStatus(false);
        $callback->setDate(new \DateTime());
        $callback->setTopListId($topListId);

        $this->entityManager->persist($callback);
        $this->entityManager->flush();

        return new JsonResponse([
            "voted" => true,
            "success" => true
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handleRuneLocusCallback(Request $request)
    {
        return $this->handleCallbackInserting(
            self::RUNELOCUS,
            $request->get('usr')
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handleTopGCallback(Request $request)
    {
        return $this->handleCallbackInserting(
            self::TOPG,
            $request->get('p_resp')
        );
    }

    public function handleRSPSListCallback(Request $request)
    {
        if (intval($request->get('voted')) == 0) {
            return new Response("NO");
        }

        return $this->handleCallbackInserting(
            self::RSPSLIST,
            $request->get('userid')
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function claimVote(Request $request)
    {
        $username = $request->get('username');
        $sessionKey = $request->get('voteSessionKey');
        $toplists = self::TOPLISTS;

        $callbacks = [];

        if ($username == null || $username == "") {
            return new JsonResponse([
                "message" => "Please enter your username"
            ]);
        }
        foreach ($toplists as $toplist) {

            /** @var Callback $callBack */
            $callBack = $this->entityManager->getRepository(Callback::class)->findOneBy([
                "sessionId" => $sessionKey,
                "topListId" => $toplist['id'],
                "status" => false
            ],['date' => 'DESC']);

            if ($callBack == null || (time() - $callBack->getDate()->getTimestamp()) > 3600 * 12) {
                continue;
            }

            $callbacks[] = $callBack;
        }

        /** @var Callback $callback */
        foreach ($callbacks as $callback) {

            $callback->setStatus(true);
            $toplistId = $callback->getTopListId();
            $toplist = $toplists[$toplistId];

            foreach ($toplist['rewards'] as $rewardId => $rewardName) {
                $voteReward = new Reward();

                $voteReward->setRewardId($rewardId);
                $voteReward->setUsername($username);

                $this->entityManager->persist($voteReward);
            }

            $this->entityManager->persist($callback);
        }

        $this->entityManager->flush();

        return new JsonResponse([
            "message" => "Use ::claim in-game to get your reward!"
        ]);
    }
}
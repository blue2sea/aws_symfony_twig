<?php
namespace WellGedaan\Indova\Application\Controller;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NoResultException;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WellGedaan\Indova\Domain\Account\TwoFactorInvite;

class SecurityController implements ControllerProviderInterface
{

    /**
     * @var \Twig_Environment
     */
    private $twig;


    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \Closure
     */
    private $lastLoginErrors;

    public function __construct(\Twig_Environment $twig, EntityManager $entityManager, \Closure $lastLoginErrors)
    {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        $this->lastLoginErrors = $lastLoginErrors;
    }

    /**
     * {@inheritdoc}
     */
    public function connect(Application $app): ControllerCollection
    {
        /** @var ControllerCollection $controllers */
        $controllers = $app['controllers_factory'];

        $controllers->get('/login', [$this, 'renderLoginPage'])->bind('security.login');
        $controllers->get('/2fa', [$this, 'handleTwoFactorAuthenticationRequest'])->bind('security.2fa');

        return $controllers;
    }


    public function renderLoginPage(Request $request): Response
    {
        $lastErrors = $this->lastLoginErrors;

        return new Response(
            $this->twig->render('security/login.html.twig', [
                "error" => $lastErrors($request),
            ])
        );
    }


    public function handleTwoFactorAuthenticationRequest(Request $request): Response
    {
        $id = $request->get('auth');

        if ($id === null) {
           return new RedirectResponse('/');
        }

        $invite = $this->getInviteById($id);

        if($invite === null) {
            return new RedirectResponse('/');
        }

        $imageURI = "https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=";
        $imageURI .= "otpauth://totp/Indova:" . $invite->getPlayerNameURL();
        $imageURI .= "&secret=" . $invite->getCode();
        $imageURI .= "&issuer=Indova";

        return new Response(
            $this->twig->render('security/2fa.html.twig', [
                'imageURI' => $imageURI
            ])
        );
    }


    private function getInviteById($id): ?TwoFactorInvite
    {
        $inviteQuery = $this->entityManager->getRepository(TwoFactorInvite::class)->createQueryBuilder('i');
        $inviteQuery->where('i.id = :id');
        $inviteQuery->andWhere('i.expiresOn > :date');

        $inviteQuery->setParameters([
            'id' => $id,
            'date' => new \DateTime()
        ]);

        try {
            return $inviteQuery->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
}
<?php
namespace WellGedaan\Indova\Application\Controller;


use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Response;

class HiscoreController implements ControllerProviderInterface
{

    /**
     * @var \Twig_Environment
     */
    private $twig;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * {@inheritdoc}
     */
    public function connect(Application $app): ControllerCollection
    {
        /** @var ControllerCollection $controllers */
        $controllers = $app['controllers_factory'];

        $controllers->get('/', [$this, 'renderHiscoreIndex'])->bind('hiscore.index');

        return $controllers;
    }

    public function renderHiscoreIndex(): Response
    {
        return  new Response(
            $this->twig->render('/hiscore/hiscore.html.twig', [
                'offset' => 0,
                'scores' => []
            ])
        );
    }

}
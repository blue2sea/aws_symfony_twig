<?php
namespace WellGedaan\Indova\Application\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Response;
use WellGedaan\Indova\Domain\Store\StoreCategory;

class StoreController implements ControllerProviderInterface
{

    const CALLBACK_PRIVATE_KEY = '3bf0d2a33c8f56ae128538a5179fddfd';

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(\Twig_Environment $twig, EntityManager $entityManager)
    {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function connect(Application $app): ControllerCollection
    {
        /** @var ControllerCollection $controllers */
        $controllers = $app['controllers_factory'];

        $controllers->get('/', [$this, 'renderStoreIndex'])->bind('store.index');
        $controllers->post('/callback236643', [$this, 'handleStoreCallback'])->bind('store.callback');

        return $controllers;
    }


    public function renderStoreIndex(): Response
    {
        $repository = $this->entityManager->getRepository(StoreCategory::class);

        return new Response(
            $this->twig->render('/store/store.html.twig', [
                'categories' => $repository->findAll()
            ])
        );
    }


    public function handleStoreCallback(): Response
    {
        return new Response('OK');
    }
}
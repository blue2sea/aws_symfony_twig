<?php
namespace WellGedaan\Indova\Application\Controller;


use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WellGedaan\Indova\Domain\ServerUpdate\ServerUpdateRepository;

class PageController implements ControllerProviderInterface
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var ServerUpdateRepository
     */
    private $serverUpdateRepository;

    public function __construct(\Twig_Environment $twig, ServerUpdateRepository $serverUpdateRepository)
    {
        $this->twig = $twig;
        $this->serverUpdateRepository = $serverUpdateRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function connect(Application $app): ControllerCollection
    {
        /** @var ControllerCollection $controllers */
        $controllers = $app['controllers_factory'];

        $controllers->get('/', [$this, 'renderHomepage'])->bind('page.home');
        $controllers->get('/landing', [$this, 'renderLandingPage'])->bind('page.landing');

        return $controllers;
    }

    public function renderHomepage(Request $request): Response
    {

        $landingCookie = $request->cookies->get('landing');

        if($landingCookie == null) {
            return new RedirectResponse('/landing');
        }

        $updates = $this->serverUpdateRepository->findLatest();

        return new Response(
            $this->twig->render('homepage.html.twig', [
                'updates' => $updates
            ])
        );
    }


    public function renderLandingPage(Request $request): Response
    {

        $response =  new Response(
            $this->twig->render('landing.html.twig')
        );

        $response->headers->setCookie(new Cookie('landing', true));

        return $response;
    }
}
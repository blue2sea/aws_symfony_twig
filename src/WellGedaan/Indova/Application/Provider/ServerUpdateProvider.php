<?php
namespace WellGedaan\Indova\Application\Provider;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
use WellGedaan\Indova\Application\Repository\ServerUpdateRepository;

class ServerUpdateProvider implements ServiceProviderInterface
{

    /**
     * {@inheritdoc}
     */
    public function register(Container $pimple)
    {
        $pimple['server.update.repository'] = function() use ($pimple) {
            return new ServerUpdateRepository;
        };
    }
}
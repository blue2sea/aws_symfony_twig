<?php
namespace WellGedaan\Indova\Application\Provider;

use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use WellGedaan\Indova\Domain\Account\Account;
use WellGedaan\Indova\Domain\Account\AccountRepository;

class UserProvider implements UserProviderInterface
{
    /**
     * @var AccountRepository
     */
    private $accountRepository;

    public function __construct(AccountRepository $accountRepository)
    {

        $this->accountRepository = $accountRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username): Account
    {
        return $this->accountRepository->findOneByUsername($username);
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user): Account
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === Account::class;
    }
}
<?php
namespace WellGedaan\Indova\Application\Repository;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use WellGedaan\Indova\Domain\ServerUpdate\ServerUpdate;
use WellGedaan\Indova\Domain\ServerUpdate\ServerUpdateRepository as ServerUpdateRepositoryInterface;

class ServerUpdateRepository implements ServerUpdateRepositoryInterface
{
    const UPDATES_URL = 'http://forum.indovaps.com/index.php?/forum/19-server-updates.xml';

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Returns the latest server updates.
     *
     * @return ServerUpdate[]
     */
    public function findLatest(): array
    {
        try {
            $response = $this->client->get(self::UPDATES_URL, [
                "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
                "Upgrade-Insecure-Requests" => 1
            ]);
        } catch (ServerException $e) {
            return [];
        }

        $data = (string) $response->getBody();
        $parsedData = simplexml_load_string($data, null, LIBXML_NOCDATA);

        if(!isset($parsedData->channel->item)) {
            return [];
        }

        $resultArray = [];
        $index = 0;

        foreach ($parsedData->channel->item as $item) {
            if($index == 4) {
                break;
            }

            $resultArray[] = ServerUpdate::fromArray([
                "guid" => (int) $item->guid,
                "title" => (string )$item->title,
                "link" => (string) $item->link,
                "description" => $this->parseContent((string) $item->description[0]),
                "date" => new \DateTime($item->pubDate)
            ]);

            $index++;
        }

        return $resultArray;
    }

    private function parseContent($rawContent): string
    {
        $content = strip_tags($rawContent, "<p><a><li><ul>");
        $content = $this->html_cut($content, 300);
        return $content;
    }

    private function html_cut($text, $max_length)
    {
        $tags   = array();
        $result = "";
        $is_open   = false;
        $grab_open = false;
        $is_close  = false;
        $in_double_quotes = false;
        $in_single_quotes = false;
        $tag = "";
        $i = 0;
        $stripped = 0;
        $stripped_text = strip_tags($text);
        while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length)
        {
            $symbol  = $text{$i};
            $result .= $symbol;
            switch ($symbol)
            {
                case '<':
                    $is_open   = true;
                    $grab_open = true;
                    break;
                case '"':
                    if ($in_double_quotes)
                        $in_double_quotes = false;
                    else
                        $in_double_quotes = true;
                    break;
                case "'":
                    if ($in_single_quotes)
                        $in_single_quotes = false;
                    else
                        $in_single_quotes = true;
                    break;
                case '/':
                    if ($is_open && !$in_double_quotes && !$in_single_quotes)
                    {
                        $is_close  = true;
                        $is_open   = false;
                        $grab_open = false;
                    }
                    break;
                case ' ':
                    if ($is_open)
                        $grab_open = false;
                    else
                        $stripped++;
                    break;
                case '>':
                    if ($is_open)
                    {
                        $is_open   = false;
                        $grab_open = false;
                        array_push($tags, $tag);
                        $tag = "";
                    }
                    else if ($is_close)
                    {
                        $is_close = false;
                        array_pop($tags);
                        $tag = "";
                    }
                    break;
                default:
                    if ($grab_open || $is_close)
                        $tag .= $symbol;
                    if (!$is_open && !$is_close)
                        $stripped++;
            }
            $i++;
        }
        foreach ($tags as $tag) {
            $result .= "</" . $tag . ">";
        }
        return $result;
    }

}
<?php
namespace WellGedaan\Indova\Application\Repository;


use Doctrine\ORM\EntityRepository;
use WellGedaan\Indova\Domain\Account\Account;
use WellGedaan\Indova\Domain\Account\AccountRepository as AccountRepositoryInterface;
use WellGedaan\Indova\Domain\Account\Exception\AccountNotFoundException;

class AccountRepository extends EntityRepository implements AccountRepositoryInterface
{

    public function findOneByUsername(string $username): ?Account
    {
        /** @var Account $account */
        $account = $this->findOneBy(['username' => $username]);

        if ($account == null) {
           throw AccountNotFoundException::forUsername($username);
        }

        return $account;
    }
}
<?php
namespace WellGedaan\Indova\Domain\Store;


/**
 * Class StoreItem
 * @package WellGedaan\Indova\Domain\Store
 *
 * @Entity
 * @Table(
 *     name="store_items"
 * )
 */
class StoreItem
{
    /**
     * @var integer
     *
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    private $order;

    /**
     * @var string
     *
     * @Column(type="string")
     */
    private $name;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    private $price;

    /**
     * @var integer
     *
     * @Column(type="integer", name="item_id")
     */
    private $itemId;


    /**
     * @var StoreCategory
     *
     * @ManyToOne(targetEntity="WellGedaan\Indova\Domain\Store\StoreCategory", inversedBy="items")
     * @JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId)
    {
        $this->itemId = $itemId;
    }

    /**
     * @return StoreCategory
     */
    public function getCategory(): StoreCategory
    {
        return $this->category;
    }

    /**
     * @param StoreCategory $category
     */
    public function setCategory(StoreCategory $category)
    {
        $this->category = $category;
    }
}

<?php
namespace WellGedaan\Indova\Domain\Store;

/**
 * Class StoreCategory
 * @package WellGedaan\Indova\Domain\Store
 *
 * @Entity
 * @Table(
 *     name="store_categories"
 * )
 */
class StoreCategory
{

    /**
     * @var integer
     *
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    private $order;

    /**
     * @var string
     *
     * @Column(type="string")
     */
    private $name;

    /**
     * @var integer
     *
     * @Column(type="integer", name="item_id")
     */
    private $itemId;

    /**
     * @var StoreItem[]
     *
     * @OneToMany(targetEntity="WellGedaan\Indova\Domain\Store\StoreItem", mappedBy="category")
     */
    private $items;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId)
    {
        $this->itemId = $itemId;
    }

    /**
     * @return StoreItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param StoreItem[] $items
     */
    public function setItems(array $items)
    {
        $this->items = $items;
    }
}
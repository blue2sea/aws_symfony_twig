<?php
namespace WellGedaan\Indova\Domain\ServerUpdate;


interface ServerUpdateRepository
{

    /**
     * Returns the latest server updates.
     *
     * @return ServerUpdate[]
     */
    public function findLatest(): array;
}
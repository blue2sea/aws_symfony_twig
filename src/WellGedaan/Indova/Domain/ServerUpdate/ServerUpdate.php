<?php
namespace WellGedaan\Indova\Domain\ServerUpdate;


class ServerUpdate
{

    /**
     * @var int
     */
    protected $guid;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $link;

    public static function fromArray(array $updateData): self
    {
        $serverUpdate = new self;

        $serverUpdate->setGuid($updateData['guid']);
        $serverUpdate->setTitle('title');
        $serverUpdate->setDate($updateData['date']);
        $serverUpdate->setDescription($updateData['description']);
        $serverUpdate->setLink($updateData['link']);

        return $serverUpdate;
    }

    private function __construct()
    {
        //use named constructors
    }

    /**
     * @return int
     */
    public function getGuid(): int
    {
        return $this->guid;
    }

    /**
     * @param int $guid
     */
    public function setGuid(int $guid): void
    {
        $this->guid = $guid;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }
}
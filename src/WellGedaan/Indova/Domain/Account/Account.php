<?php
namespace WellGedaan\Indova\Domain\Account;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class Account
 * @package WellGedaan\Indova\Domain\Account
 *
 * @Table(
 *     name="accounts"
 * )
 * @Entity(
 *     repositoryClass="WellGedaan\Indova\Application\Repository\AccountRepository"
 * )
 */
class Account implements UserInterface
{

    /**
     * @var int
     *
     * @Column(type="integer")
     * @Id
     * @GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="string")
     */
    private $username;

    /**
     * @var string
     *
     * @Column(type="string")
     */
    private $password;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    private $rights;

    /**
     * @var string
     *
     * @Column(type="string")
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", name="muted_until")
     */
    private $mutedUntil;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", name="banned_until")
     */
    private $bannedUntil;

    /**
     * @var string
     *
     * @Column(type="string")
     */
    private $displayName;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", name="premium_until")
     */
    private $premiumUntil;

    /**
     * @var string
     *
     * @Column(type="string")
     */
    private $lastname;

    /**
     * @var integer
     *
     * @Column(type="integer", name="forum_id")
     */
    private $forumId;

    /**
     * @var integer
     *
     * @Column(type="integer", name="store_credits")
     */
    private $storeCredits;

    /**
     * @var integer
     *
     * @Column(type="integer", name="premium_rank")
     */
    private $premium_rank;

    /**
     * @var string
     *
     * @Column(type="string")
     */
    private $clanname;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    private $joinreq;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    private $talkreq;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    private $kickreq;

    /**
     * @var boolean
     *
     * @Column(type="boolean")
     */
    private $logged;

    /**
     * @var integer
     *
     * @Column(type="integer", name="default_icon")
     */
    private $defaultIcon;

    /**
     * @var string
     *
     * @Column(type="string", name="twofactor_key")
     */
    private $twofactorKey;

    /**
     * @var string
     *
     * @Column(type="string", name="last_ip")
     */
    private $lastIp;

    /**
     * @var string
     *
     * @Column(type="string", name="last_hwid")
     */
    private $lastHwid;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", name="last_online")
     */
    private $lastOnline;

    /**
     * @var string
     *
     * @Column(type="string", name="last_uuid")
     */
    private $lastUUID;

    /**
     * @var boolean
     *
     * @Column(type="boolean", name="deadman_participant")
     */
    private $deadmanParticipant;

    /**
     * @var float
     *
     * @Column(type="float", name="extra_usd")
     */
    private $extraUSD;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }

    /**
     * @return int
     */
    public function getRights(): ?int
    {
        return $this->rights;
    }

    /**
     * @param int $rights
     */
    public function setRights(int $rights)
    {
        $this->rights = $rights;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return \DateTime
     */
    public function getMutedUntil(): ?\DateTime
    {
        return $this->mutedUntil;
    }

    /**
     * @param \DateTime $mutedUntil
     */
    public function setMutedUntil(\DateTime $mutedUntil)
    {
        $this->mutedUntil = $mutedUntil;
    }

    /**
     * @return \DateTime
     */
    public function getBannedUntil(): ?\DateTime
    {
        return $this->bannedUntil;
    }

    /**
     * @param \DateTime $bannedUntil
     */
    public function setBannedUntil(\DateTime $bannedUntil)
    {
        $this->bannedUntil = $bannedUntil;
    }

    /**
     * @return string
     */
    public function getDisplayName(): ?string
    {
        if($this->displayName == null) {
            return $this->username;
        }

        return $this->displayName;
    }

    /**
     * @param string $displayName
     */
    public function setDisplayName(string $displayName)
    {
        $this->displayName = $displayName;
    }

    /**
     * @return \DateTime
     */
    public function getPremiumUntil(): ?\DateTime
    {
        return $this->premiumUntil;
    }

    /**
     * @param \DateTime $premiumUntil
     */
    public function setPremiumUntil(\DateTime $premiumUntil)
    {
        $this->premiumUntil = $premiumUntil;
    }

    /**
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname(string $lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return int
     */
    public function getForumId(): ?int
    {
        return $this->forumId;
    }

    /**
     * @param int $forumId
     */
    public function setForumId(int $forumId)
    {
        $this->forumId = $forumId;
    }

    /**
     * @return int
     */
    public function getStoreCredits(): ?int
    {
        return $this->storeCredits;
    }

    /**
     * @param int $storeCredits
     */
    public function setStoreCredits(int $storeCredits)
    {
        $this->storeCredits = $storeCredits;
    }

    /**
     * @return int
     */
    public function getPremiumRank(): ?int
    {
        return $this->premium_rank;
    }

    /**
     * @param int $premium_rank
     */
    public function setPremiumRank(int $premium_rank)
    {
        $this->premium_rank = $premium_rank;
    }

    /**
     * @return string
     */
    public function getClanname(): ?string
    {
        return $this->clanname;
    }

    /**
     * @param string $clanname
     */
    public function setClanname(string $clanname)
    {
        $this->clanname = $clanname;
    }

    /**
     * @return int
     */
    public function getJoinreq(): ?int
    {
        return $this->joinreq;
    }

    /**
     * @param int $joinreq
     */
    public function setJoinreq(int $joinreq)
    {
        $this->joinreq = $joinreq;
    }

    /**
     * @return int
     */
    public function getTalkreq(): ?int
    {
        return $this->talkreq;
    }

    /**
     * @param int $talkreq
     */
    public function setTalkreq(int $talkreq)
    {
        $this->talkreq = $talkreq;
    }

    /**
     * @return int
     */
    public function getKickreq(): ?int
    {
        return $this->kickreq;
    }

    /**
     * @param int $kickreq
     */
    public function setKickreq(int $kickreq)
    {
        $this->kickreq = $kickreq;
    }

    /**
     * @return bool
     */
    public function isLogged(): bool
    {
        return $this->logged;
    }

    /**
     * @param bool $logged
     */
    public function setLogged(bool $logged)
    {
        $this->logged = $logged;
    }

    /**
     * @return int
     */
    public function getDefaultIcon(): ?int
    {
        return $this->defaultIcon;
    }

    /**
     * @param int $defaultIcon
     */
    public function setDefaultIcon(int $defaultIcon)
    {
        $this->defaultIcon = $defaultIcon;
    }

    /**
     * @return string
     */
    public function getTwofactorKey(): ?string
    {
        return $this->twofactorKey;
    }

    /**
     * @param string $twofactorKey
     */
    public function setTwofactorKey(string $twofactorKey)
    {
        $this->twofactorKey = $twofactorKey;
    }

    /**
     * @return string
     */
    public function getLastIp(): ?string
    {
        return $this->lastIp;
    }

    /**
     * @param string $lastIp
     */
    public function setLastIp(string $lastIp)
    {
        $this->lastIp = $lastIp;
    }

    /**
     * @return string
     */
    public function getLastHwid(): ?string
    {
        return $this->lastHwid;
    }

    /**
     * @param string $lastHwid
     */
    public function setLastHwid(string $lastHwid)
    {
        $this->lastHwid = $lastHwid;
    }

    /**
     * @return \DateTime
     */
    public function getLastOnline(): ?\DateTime
    {
        return $this->lastOnline;
    }

    /**
     * @param \DateTime $lastOnline
     */
    public function setLastOnline(\DateTime $lastOnline)
    {
        $this->lastOnline = $lastOnline;
    }

    /**
     * @return string
     */
    public function getLastUUID(): ?string
    {
        return $this->lastUUID;
    }

    /**
     * @param string $lastUUID
     */
    public function setLastUUID(string $lastUUID)
    {
        $this->lastUUID = $lastUUID;
    }

    /**
     * @return bool
     */
    public function isDeadmanParticipant(): bool
    {
        return $this->deadmanParticipant;
    }

    /**
     * @param bool $deadmanParticipant
     */
    public function setDeadmanParticipant(bool $deadmanParticipant)
    {
        $this->deadmanParticipant = $deadmanParticipant;
    }

    /**
     * @return float
     */
    public function getExtraUSD(): ?float
    {
        return $this->extraUSD;
    }

    /**
     * @param float $extraUSD
     */
    public function setExtraUSD(float $extraUSD)
    {
        $this->extraUSD = $extraUSD;
    }
}
<?php
namespace WellGedaan\Indova\Domain\Account\Exception;


use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class AccountNotFoundException extends UsernameNotFoundException
{
    public static function forUsername(string $username): self
    {
        $exception = new self();
        $exception->setUsername($username);

        return $exception;
    }
}
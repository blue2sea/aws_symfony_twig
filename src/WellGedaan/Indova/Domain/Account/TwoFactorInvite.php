<?php
namespace WellGedaan\Indova\Domain\Account;

/**
 * Class TwoFactorInvite
 * @package WellGedaan\Indova\Domain\Account
 *
 * @Entity
 * @Table(
 *     name="twofactor_invites"
 * )
 */
class TwoFactorInvite
{

    /**
     * @var string
     *
     * @Id
     * @GeneratedValue
     * @Column(type="string")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="string")
     */
    private $code;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", name="generated_on")
     */
    private $generatedOn;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", name="expires_on")
     */
    private $expiresOn;

    /**
     * @var string
     *
     * @Column(type="string", name="player_name")
     */
    private $playerName;

    /**
     * @var string
     *
     * @Column(type="string", name="player_name_url")
     */
    private $playerNameURL;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * @return \DateTime
     */
    public function getGeneratedOn(): \DateTime
    {
        return $this->generatedOn;
    }

    /**
     * @param \DateTime $generatedOn
     */
    public function setGeneratedOn(\DateTime $generatedOn)
    {
        $this->generatedOn = $generatedOn;
    }

    /**
     * @return \DateTime
     */
    public function getExpiresOn(): \DateTime
    {
        return $this->expiresOn;
    }

    /**
     * @param \DateTime $expiresOn
     */
    public function setExpiresOn(\DateTime $expiresOn)
    {
        $this->expiresOn = $expiresOn;
    }

    /**
     * @return string
     */
    public function getPlayerName(): string
    {
        return $this->playerName;
    }

    /**
     * @param string $playerName
     */
    public function setPlayerName(string $playerName)
    {
        $this->playerName = $playerName;
    }

    /**
     * @return string
     */
    public function getPlayerNameURL(): string
    {
        return $this->playerNameURL;
    }

    /**
     * @param string $playerNameURL
     */
    public function setPlayerNameURL(string $playerNameURL)
    {
        $this->playerNameURL = $playerNameURL;
    }
}
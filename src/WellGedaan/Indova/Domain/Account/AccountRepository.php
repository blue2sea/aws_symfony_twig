<?php
namespace WellGedaan\Indova\Domain\Account;


interface AccountRepository
{
    public function findOneByUsername(string $username): ?Account;
}
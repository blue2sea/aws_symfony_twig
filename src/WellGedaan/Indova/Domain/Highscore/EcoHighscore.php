<?php
namespace WellGedaan\Indova\Domain\Highscore;
use WellGedaan\Indova\Domain\Account\Account;


/**
 * Class EcoHighscore
 * @package WellGedaan\Indova\Domain\Highscore
 *
 * @Entity
 * @Table(
 *     name="eco_highscores"
 * )
 */
class EcoHighscore
{

    /**
     * @var Account
     *
     * @Id
     *
     * @ManyToOne(targetEntity="WellGedaan\Indova\Domain\Account\Account"
     * @JoinColumn(name="account_id", referencedColumnName="id"))
     */
    protected $account;

    /**
     * @var integer
     *
     * @Id
     * @Column(type="integer")
     */
    protected $skill;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    protected $character_id;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    protected $xp;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime")
     */
    protected $last_updated;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    protected $game_mode;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    protected $service_id;

    /**
     * @var integer
     */
    protected $lvl;
}
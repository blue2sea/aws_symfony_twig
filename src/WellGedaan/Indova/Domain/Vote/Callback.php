<?php
namespace WellGedaan\Indova\Domain\Vote;

/**
 * Class Callback
 * @package WellGedaan\Runique\Model\Vote
 *
 * @Entity
 * @Table(name="callback")
 * @Cache(usage="NONSTRICT_READ_WRITE")
 */
class Callback
{
    /**
     * @var int
     *
     * @id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", name="session_id")
     */
    protected $sessionId;

    /**
     * @var int
     *
     * @Column(type="integer", name="toplist_id")
     */
    protected $topListId;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime")
     */
    protected $date;

    /**
     * @var bool
     *
     * @Column(type="boolean")
     */
    protected $status;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId(string $sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return int
     */
    public function getTopListId(): int
    {
        return $this->topListId;
    }

    /**
     * @param int $topListId
     */
    public function setTopListId(int $topListId)
    {
        $this->topListId = $topListId;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

}
<?php
namespace WellGedaan\Indova\Domain\Vote;

/**
 * Class Reward
 * @package WellGedaan\Indova\Domain\Vote
 *
 * @Entity
 * @Table(name="vote_reward")
 */
class Reward
{

    /**
     * @var int
     *
     * @id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string")
     */
    protected $username;

    /**
     * @var int
     *
     * @Column(type="integer", name="reward_id")
     */
    protected $rewardId = 979;

    /**
     * @var string
     *
     * @Column(type="string")
     */
    protected $status = "false";

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    /**
     * @return int
     */
    public function getRewardId(): ?int
    {
        return $this->rewardId;
    }

    /**
     * @param int $rewardId
     */
    public function setRewardId(int $rewardId)
    {
        $this->rewardId = $rewardId;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }
}
<?php
namespace WellGedaan\Indova\Domain\OnlineStatistics;


/**
 * Class OnlineStatistics
 * @package WellGedaan\Indova\Domain\OnlineStatistics
 *
 * @Entity
 * @Table(
 *     name="online_statistics"
 * )
 */
class OnlineStatistics
{

    /**
     * @var int
     *
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime")
     */
    private $time;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    private $world;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    private $players;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getTime(): ?\DateTime
    {
        return $this->time;
    }

    /**
     * @param \DateTime $time
     */
    public function setTime(\DateTime $time)
    {
        $this->time = $time;
    }

    /**
     * @return int
     */
    public function getWorld(): ?int
    {
        return $this->world;
    }

    /**
     * @param int $world
     */
    public function setWorld(int $world)
    {
        $this->world = $world;
    }

    /**
     * @return int
     */
    public function getPlayers(): ?int
    {
        return $this->players;
    }

    /**
     * @param int $players
     */
    public function setPlayers(int $players)
    {
        $this->players = $players;
    }
}
<?php
namespace WellGedaan\Indova\Infrastructure\ServiceProvider;


use Igorw\Silex\YamlConfigDriver;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ConfigServiceProvider implements ServiceProviderInterface
{


    /**
     * {@inheritdoc}
     */
    public function register(Container $pimple)
    {
        $pimple['env'] = $this->getEnv($pimple);

        $pimple->register(new \Igorw\Silex\ConfigServiceProvider(
            APP_PATH . "/config/config.{$pimple['env']}.yml",
            [],
            new YamlConfigDriver,
            "config"
        ));
    }
    public function getEnv(Container $pimple): string
    {
        if($pimple['debug'] == false && getenv('TEST')) {
            throw new \RuntimeException("NO! NEVER RUN TESTS WHILE ENV IS ON PROD!!");
        }
        if ($pimple['debug'] && getenv('TRAVIS')) {
            return 'test';
        }
        if ($pimple['debug']) {
            return 'dev';
        }
        return 'prod';
    }
}
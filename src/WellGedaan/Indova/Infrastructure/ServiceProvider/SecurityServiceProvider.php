<?php
namespace WellGedaan\Indova\Infrastructure\ServiceProvider;


use Doctrine\ORM\EntityManager;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Provider\SessionServiceProvider;
use WellGedaan\Indova\Application\Provider\UserProvider;
use WellGedaan\Indova\Domain\Account\Account;
use WellGedaan\Indova\Domain\Account\AccountRepository;

class SecurityServiceProvider implements ServiceProviderInterface
{

    /**
     * {@inheritdoc}
     */
    public function register(Container $pimple)
    {
        $pimple->register(new SessionServiceProvider);

        $pimple['UserProvider'] = function () use ($pimple) {
            /** @var EntityManager $entityManager */
            $entityManager = $pimple['orm.em'];

            /** @var AccountRepository $accountRepository */
            $accountRepository = $entityManager->getRepository(Account::class);

            return new UserProvider($accountRepository);
        };

        $pimple->register(new \Silex\Provider\SecurityServiceProvider(), array(
            'security.firewalls' => array(
                'login_path' => array(
                    'pattern' => '^/login$',
                    'anonymous' => true
                ),
                'default' => array(
                    'pattern' => '^/.*$',
                    'anonymous' => true,
                    'logout' => array(
                        'logout_path' => '/security/logout',
                    ),
                    'form' => array(
                        'login_path' => '/security/login',
                        'check_path' => '/security/login_check'
                    ),
                    'users' => function ($app) {
                        return $app['UserProvider'];
                    },
                ),
            ),
            'security.access_rules' => []
        ));
    }
}
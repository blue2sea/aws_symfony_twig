<?php
namespace WellGedaan\Indova\Infrastructure\ServiceProvider;


use Doctrine\ORM\EntityManager;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use WellGedaan\Indova\Domain\OnlineStatistics\OnlineStatistics;

class TwigServiceProvider implements ServiceProviderInterface
{

    /**
     * {@inheritdoc}
     */
    public function register(Container $pimple)
    {
        $pimple->register(new \Silex\Provider\TwigServiceProvider(), [
            'twig.path' => VIEW_PATH,
        ]);



        $pimple->extend('twig', function(\Twig_Environment $twig) use ($pimple) {

            $twig->addFunction(new \Twig_Function('players_online', function() use ($pimple) {
                /** @var EntityManager $em */
                $em = $pimple['orm.em'];

                /** @var OnlineStatistics $onlineStatistic */
                $onlineStatistic = $em->getRepository(OnlineStatistics::class)->findOneBy([], ['id' => 'DESC']);

                return $onlineStatistic->getPlayers();
            }));

            return $twig;
        });
    }
}
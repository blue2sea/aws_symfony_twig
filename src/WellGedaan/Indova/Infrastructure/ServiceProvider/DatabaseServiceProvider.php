<?php
namespace WellGedaan\Indova\Infrastructure\ServiceProvider;

use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use MartinGeorgiev\Doctrine\DBAL\Types\BigIntArray;
use MartinGeorgiev\Doctrine\DBAL\Types\IntegerArray;
use MartinGeorgiev\Doctrine\DBAL\Types\Jsonb;
use MartinGeorgiev\Doctrine\DBAL\Types\JsonbArray;
use MartinGeorgiev\Doctrine\DBAL\Types\SmallIntArray;
use MartinGeorgiev\Doctrine\DBAL\Types\TextArray;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Provider\DoctrineServiceProvider;

class DatabaseServiceProvider implements ServiceProviderInterface
{

    /**
     * {@inheritdoc}
     */
    public function register(Container $pimple)
    {
        $this->registerDatabase($pimple);
        $this->registerORM($pimple);
    }
    /**
     * Registers the main database.
     *
     * @param Container $pimple
     */
    private function registerDatabase(Container $pimple)
    {
        $pimple->register(new DoctrineServiceProvider(), array(
            'db.options' => $pimple['config']['database']
        ));
    }
    /**
     * Registers the Doctrine ORM service provider.
     *
     * @param Container $pimple
     */
    private function registerORM(Container $pimple)
    {
        $pimple->register(new DoctrineOrmServiceProvider, array(
            'orm.default_cache' => array(
                'driver' => "filesystem",
                'path' => CACHE_PATH . "/ORM"
            ),
            'orm.em.options' => array(
                'mappings' => array(
                    array(
                        'type' => 'annotation',
                        'namespace' => 'WellGedaan\Indova\Domain',
                        'path' => APP_PATH.'/../src/WellGedaan/Indova/Domain',
                    )
                ),
                'types' => [
                    'jsonb' => Jsonb::class,
                    'jsonb[]' => JsonbArray::class,
                    'smallint[]' => SmallIntArray::class,
                    'integer[]' => IntegerArray::class,
                    'bigint[]' => BigIntArray::class,
                    'text[]' => TextArray::class,
                    '_int4' => IntegerArray::class
                ]
            ),
        ));
    }

}